#include<bits/stdc++.h>
using namespace std;

class node{
    public:
        int val;
        node *left, *right;
        node(int v) : val(v), left(nullptr), right(nullptr){};
};

int height(node *root){

    if(root == nullptr){
        return 0;
    }
    if(root->left == nullptr && root->right == nullptr)
        return 1;

    int left = height(root->left);
    int right = height(root->right);

    return 1 + max(left, right);
}

bool areMirror(node * root1, node *root2){
    if(root1 == nullptr && root2 == nullptr){
        return true;
    }
    if(root1 ==nullptr || root2 == nullptr)return false;

    return (root1->val == root2->val && areMirror(root1->left,root2->right) && areMirror(root1->right,root2->left));

}


int main(){
    node *root = new node(15);
    root->left = new node(10);
    root->right = new node(23);

    root->left->left = new node(9);
    root->left->right = new node(12);

    root->right->left = new node(16);
    root->right->right = new node(26); 

    root->right->right->left = new node(20);

    //print height of the tree.
    int h = height(root);
    cout << h << " ";

    return 0;
}
